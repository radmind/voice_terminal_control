#!/bin/bash
tempc=/tmp/voiceecho.txt
trap 'rm -f "$tempc" ; echo -en "\nRemoving temp files..." ;sleep 1 ; exit ' 0 15 #clean on SIGTERM
while [ 0 -eq 0 ]
do
while read j
	do
		tempcmd=`tail -1 "$tempc" | sed 's/dash\ /\-/g' | tr "[:upper:]" "[:lower:]"`
done < <(inotifywait -q -e modify "$tempc")

echo "do you really want to exec "$tempcmd" ?"

while read $confcmd
	do
		cnf=`tail -1 "$tempc"`
done < <(inotifywait -q -e modify "$tempc")
if [ "$cnf" = "yes" ]
then
	echo "$tempcmd" | bash 
else
       	echo "not executing"
	echo "$cnf"
fi

done
